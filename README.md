# Angular Test Employee

## Guide to run the project
1. After `git clone` use branch `main`. Install all package with `npm install` 
```
npm install
```
2. Create file `environment.ts`, copy and paste this code below.
```
export const environment = {
  production: false,
  locale: 'id-ID',
  currency: 'IDR',
  apiUrl: 'http://localhost:3000/'
};
```
3. For fake API using `typicode/json-server`. Run `npx json-server employee.json`. The API will automatically ready to use 
```
npx json-server employee.json
```
4. Run `ng serve` on a **new terminal** for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
```
ng serve
```
5. Login using 'user' & 'user' as credential. The username and password are same word `user`.

## Further help
Please do contact me if there's any problem with the project. Thank you 
