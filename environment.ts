export const environment = {
  production: false,
  locale: 'id-ID',
  currency: 'IDR',
  apiUrl: 'http://localhost:3000/'
};