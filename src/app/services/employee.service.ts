import { Employee, ResponseBody } from './../interfaces/employee';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environment';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  constructor(private http: HttpClient) { }

  getEmployee() {
    return this.http.get<Employee[]>(`${environment.apiUrl}employee`);
  }

  getDetailEmployee(id: string) {
    return this.http.get<Employee | null>(`${environment.apiUrl}employee/` + id);
  }

  addEmployee(payload: Employee) {

    const transformedPayload = {
      username: payload.username,
      firstName: payload.firstName,
      lastName: payload.lastName,
      email: payload.email,
      birthDate: payload.birthDate,
      basicSalary: payload.basicSalary,
      status: payload.status,
      group: payload.group,
      description: payload.description
    };

    return this.http.post(`${environment.apiUrl}employee`, transformedPayload);
  }

  updateEmployee(payload: Employee, id?: string) {
    const transformedPayload = {
      id: id,
      username: payload.username,
      firstName: payload.firstName,
      lastName: payload.lastName,
      email: payload.email,
      birthDate: payload.birthDate,
      basicSalary: payload.basicSalary,
      status: payload.status,
      group: payload.group,
      description: payload.description
    };

    return this.http.put(`${environment.apiUrl}employee/` + id, transformedPayload);
  }

  deleteEmployee(id: string): Observable<any> {
    return this.http.delete(`${environment.apiUrl}employee/` + id);
  }

  removeCommas(inputString: string): string {
    return inputString.replace(/,/g, '');
  }

  formatSalaryInput(inputValue: string): string {
    if (inputValue === '-') return '';

    let commasRemoved = inputValue.replace(/,/g, '');
    let toInt: number;
    let toLocale: string;

    if (commasRemoved.split('.').length > 1) {
      let decimal = isNaN(parseInt(commasRemoved.split('.')[1])) ? '' : parseInt(commasRemoved.split('.')[1]);
      toInt = parseInt(commasRemoved);
      toLocale = toInt.toLocaleString('en-US') + '.' + decimal;
    } else {
      toInt = parseInt(commasRemoved);
      toLocale = toInt.toLocaleString('en-US');
    }

    return toLocale === 'NaN' ? '' : toLocale;
  }

}
