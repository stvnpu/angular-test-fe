import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Employee } from '../interfaces/employee';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private searchKeywordSubject = new BehaviorSubject<string>('');
  searchKeyword$ = this.searchKeywordSubject.asObservable();
  
  private limitSubject = new BehaviorSubject<number>(10);
  limitSubject$ = this.limitSubject.asObservable();

  private searchResultsSubject = new BehaviorSubject<Employee[]>([]);
  searchResults$ = this.searchResultsSubject.asObservable();

  constructor() { }

  setSearchResults(keyword: string, limit: number, results: Employee[]): void {
    this.searchKeywordSubject.next(keyword);
    this.limitSubject.next(limit);
    this.searchResultsSubject.next(results);
  }
}
