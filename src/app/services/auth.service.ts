import { DetailUser, User } from './../interfaces/user';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject = new BehaviorSubject<DetailUser | null>(null);
  userSubject$ = this.userSubject.asObservable();
  constructor() { }


  setUser(user: DetailUser | null) {
    this.userSubject.next(user);
  }

  unsetUser() {
    this.setUser(null);
  }

  loginUser(data: User){
    return  data.username == 'user' && data.password == 'user';
  }
}
