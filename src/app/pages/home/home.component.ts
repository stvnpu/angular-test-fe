import { ToastrService } from 'ngx-toastr';
import { Observable, combineLatest } from 'rxjs';
import { AfterViewChecked, Component, NgModule, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../interfaces/employee';
import { CommonModule } from '@angular/common';
import { CardComponent } from '../../components/shared/card/card.component';
import { Router, RouterLink } from '@angular/router';
import { FormsModule, NgModel } from '@angular/forms';
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, CardComponent, RouterLink, FormsModule,],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  employee: Employee[] = [];
  searchQuery = '';
  limit = 10;
  limitOptions = [10, 20, 40, 80, 100];
  currentPage = 1;
  totalPages = 0;
  totalData = 0;
  sortBy = '';
  sortOrder: 'asc' | 'desc' = 'asc';
  paginatedData: Employee[] = [];

  constructor(
    private employeeService: EmployeeService,
    private toastService: ToastrService,
    private searchService: SearchService,) { }

  ngOnInit() {
    combineLatest([
      this.searchService.searchKeyword$,
      this.searchService.searchResults$,
      this.searchService.limitSubject$,
    ]).subscribe(
      ([keyword, results, limit]) => {
        this.searchQuery = keyword;
        this.paginatedData = results;
        this.limit = Number(limit);

        this.applyFilters();
      }
    )
    this.getData();
  }

  onLimitChange() {
    this.currentPage = 1;
    this.applyFilters();
  }

  getData() {
    this.employeeService.getEmployee().subscribe(
      (res: Employee[]) => {
        this.employee = res;
        this.totalData = this.employee.length;
        this.totalPages = Math.ceil(this.employee.length / this.limit);
        this.applyFilters();
      },
      error => {
        console.error('Error fetching employees:', error);
      }
    );
  }

  delete(id: string) {
    this.employeeService.deleteEmployee(id).subscribe(
      () => {
        this.toastService.success('Data Deleted !', 'Success');
        this.getData();
      },
      error => {
        console.error('Error deleting employee:', error);
      }
    );
  }

  sortData(field: string) {
    if (this.sortBy === field) {
      this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortBy = field;
      this.sortOrder = 'asc';
    }
    this.employee.sort((a: any, b: any) => {
      const comparison = this.sortOrder === 'asc' ? 1 : -1;
      return a[field].localeCompare(b[field]) * comparison;
    });
    this.applyFilters();
  }

  goToPage(page: number) {
    if (page < 1 || page > this.totalPages) {
      return;
    }
    this.currentPage = page;
    this.applyFilters();
  }

  updatePagination(filteredData: Employee[]) {
    const startIndex = (this.currentPage - 1) * this.limit;
    let endIndex = startIndex + Number(this.limit);
    if (endIndex > filteredData.length) {
      endIndex = filteredData.length;
    }
    this.paginatedData = filteredData.slice(startIndex, endIndex);
  }

  applyFilters() {
    let filteredData = this.employee.slice();

    if (this.searchQuery) {
      const query = this.searchQuery.toLowerCase().trim();
      filteredData = filteredData.filter(item =>
        Object.values(item).some(val => {
          if (typeof val === 'string') {
            return val.toLowerCase().includes(query);
          }
          return false;
        })
      );
    }

    if (this.sortBy) {
      const comparison = this.sortOrder === 'asc' ? 1 : -1;
      filteredData.sort((a, b) =>
        a[this.sortBy].localeCompare(b[this.sortBy]) * comparison
      );
    }

    this.totalPages = Math.ceil(filteredData.length / this.limit);

    if (this.currentPage > this.totalPages) {
      this.currentPage = this.totalPages;
    } else if (this.currentPage < 1) {
      this.currentPage = 1;
    }

    this.updatePagination(filteredData);
    this.searchService.setSearchResults(this.searchQuery, this.limit, filteredData);
  }

}
