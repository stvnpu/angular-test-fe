import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DatePipe, NgClass, NgFor } from '@angular/common';
import { ErrorMessagesComponent } from '../../../components/error-messages/error-messages.component';
import { FormControlOption } from '../../../interfaces/form-control-option';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { provideNativeDateAdapter } from '@angular/material/core';
import { NumberSeparatorDirective } from '../../../directives/number-separator.directive';
import { EmployeeService } from '../../../services/employee.service';
import { Employee } from '../../../interfaces/employee';

type FormFields =
  'email'
  | 'firstName'
  | 'lastName'
  | 'basicSalary'
  | 'birthDate'
  | 'group'
  | 'description'
  | 'status';

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [
    ReactiveFormsModule, 
    FormsModule, 
    NgFor, 
    NgClass, 
    ErrorMessagesComponent, 
    MatFormFieldModule, 
    MatInputModule, 
    MatDatepickerModule, 
    RouterLink,
    DatePipe,
    NumberSeparatorDirective],
  providers: [provideNativeDateAdapter(), DatePipe],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.scss'
})
export class EditEmployeeComponent implements OnInit {
  id: string = '';
  active: boolean = true;
  form: FormGroup;
  maxDate = new Date();
  formControlOption: Record<FormFields, FormControlOption> = {
    email: {
      serverField: 'email',
      rules: [
        {
          name: 'required',
          message: 'Email required',
        },
        {
          name: 'pattern',
          message: 'Email formats should be valid',
        },
      ],
    },
    firstName: {
      serverField: 'firstName',
      rules: [
        {
          name: 'required',
          message: 'First name required',
        },
      ],
    },
    lastName: {
      serverField: 'lastName',
      rules: [
        {
          name: 'required',
          message: 'Last name required',
        },
      ],
    },
    basicSalary: {
      serverField: 'basicSalary',
      rules: [
        {
          name: 'required',
          message: 'Salary required',
        },
        {
          name: 'pattern',
          message: 'Only number allowed'
        }
      ],
    },
    birthDate: {
      serverField: 'birthDate',
      rules: [
        {
          name: 'required',
          message: 'Birth Date required',
        },
      ],
    },
    group: {
      serverField: 'group',
      rules: [
        {
          name: 'required',
          message: 'Group required',
        },
      ],
    },
    status: {
      serverField: 'status',
      rules: [
        {
          name: 'required',
          message: 'Group required',
        },
      ],
    },
    description: {
      serverField: 'description',
      rules: [
        {
          name: 'required',
          message: 'Description required',
        },
      ],
    },


  };
  groupOption = [{ value: 'Alpha' }, { value: 'Beta' }, { value: 'Delta' }, { value: 'Iota' }, { value: 'Kappa' }, { value: 'Epsilon' }, { value: 'Eta' }, { value: 'Zeta' }, { value: 'Gamma' }, { value: 'Theta' }];
  statusOption = [{ value: 'Intern' }, { value: 'Permanent' }];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private employeeService: EmployeeService,
    private datePipe : DatePipe,
  ) {
    this.form = new FormGroup({
      email: new FormControl(null, {
        validators: [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")],
      }),
      firstName: new FormControl(null, {
        validators: [Validators.required],
      }),
      lastName: new FormControl(null, {
        validators: [Validators.required],
      }),
      basicSalary: new FormControl(null, {
        validators: [Validators.required, Validators.pattern("^[0-9,]+$")],
      }),
      status: new FormControl('Intern', {
        validators: [Validators.required],
      }),
      description: new FormControl(null, {
        validators: [Validators.required],
      }),
      group: new FormControl('Alpha', {
        validators: [Validators.required],
      }),
      birthDate: new FormControl(null, {
        validators: [Validators.required],
      }),
    });
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.employeeService.getDetailEmployee(this.id).subscribe({
      next: response => {
        if (response) {
          this.form.patchValue({
            firstName: response.firstName,
            lastName: response.lastName,
            email: response.email,
            birthDate: response.birthDate ? new Date(response.birthDate) : null,
            basicSalary: response.basicSalary ? this.employeeService.formatSalaryInput(response.basicSalary.toString()) : '',
            group: response.group,
            status: response.status,
            description: response.status,
          });
        }
      },
      error: error => {
        this.toast.error('Server Error, please do contact Admin', 'Error');
        console.log(error);
      }
    });
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    const birthDateControl = this.form.get('birthDate');
    const formattedBirthDate = birthDateControl && birthDateControl.value
      ? this.datePipe.transform(birthDateControl.value, 'yyyy-MM-ddTHH:mm:ss') || ''
      : '';
    const payload: Employee = {
      id: this.id,
      username: this.form.get('firstName')?.value.toLowerCase() + this.form.get('lastName')?.value.toLowerCase(),
      firstName: this.form.get('firstName')?.value,
      lastName: this.form.get('lastName')?.value,
      email: this.form.get('email')?.value,
      basicSalary: Number(this.employeeService.removeCommas(this.form.get('basicSalary')?.value)),
      birthDate: formattedBirthDate,
      group: this.form.get('group')?.value,
      status: this.form.get('status')?.value,
      description: this.form.get('description')?.value
    }

    this.employeeService.updateEmployee(payload, this.id).subscribe(
      {
        next: response => {
          this.toast.success('Data Saved', 'Success');
          console.log(response);
          this.router.navigate(['/home']);
        },
        error: error => {
          this.toast.error('Server Error, please do contact Admin', 'Error');
          console.log(error);
        },
      }
    );
  }

}
