import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DatePipe, NgIf } from '@angular/common';
import { provideNativeDateAdapter } from '@angular/material/core';
import { BetterCurrencyPipe } from '../../../pipes/better-currency.pipe';
import { EmployeeService } from '../../../services/employee.service';
import { Employee } from '../../../interfaces/employee';
@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [  DatePipe, RouterLink, NgIf, BetterCurrencyPipe],
  providers: [provideNativeDateAdapter(), BetterCurrencyPipe, DatePipe],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss'
})
export class DetailEmployeeComponent implements OnInit {
  id: string = '';
  detailEmployee: Employee | null;       

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private employeeService: EmployeeService,) { }

  public ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.employeeService.getDetailEmployee(this.id).subscribe({
      next: response => {
        if (response) {
          this.detailEmployee = response;
        }
      },
      error: error => {
        this.toast.error('Server Error, please do contact Admin', 'Error');
        console.log(error);
      },
      complete() {
        console.log("success");
      },
    });
  }

  goBack() {
    this.router.navigate(['/home']);
  }

}

