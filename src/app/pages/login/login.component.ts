import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { User } from '../../interfaces/user';
import { FormControlOption } from '../../interfaces/form-control-option';
import { ErrorMessagesComponent } from '../../components/error-messages/error-messages.component';
import { TitleCasePipe } from '@angular/common';

type FormFields = 'username' | 'password';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule,ErrorMessagesComponent,TitleCasePipe],
  providers:[TitleCasePipe],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit{
  active: boolean = true;

  form: FormGroup;

  formControlOption: Record<FormFields, FormControlOption> = {
    username: {
      serverField: 'username',
      rules: [
        {
          name: 'required',
          message: 'Username required',
        },
      ],
    },
    password: {
      serverField: 'password',
      rules: [
        {
          name: 'required',
          message: 'Password required',
        },
      ],
    },
   


  };

  constructor(
    private router: Router,
    private toast: ToastrService,
    private authService: AuthService,
    private titlePipe : TitleCasePipe
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
    username: new FormControl(null, {
      validators: [Validators.required],
    }),
    password: new FormControl(null, {
      validators: [Validators.required],
    }),
    });
    this.form.patchValue({
      username: "user",
      password: "user",
    });
  }
  
  viewPassword() {
    this.active = !this.active;
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const payload : User={
      username: this.form.get('username')?.value ?? '',
      password: this.form.get('password')?.value ?? ''
    }
    
    if (this.authService.loginUser(payload)) {
      this.authService.setUser({ username: payload.username })
      this.authService.userSubject$.subscribe(
        (next) => {
          this.toast.success('Welcome, ' + this.titlePipe.transform(next?.username), 'Success');
          this.router.navigate(['/home']);
        },
        error=>{
          this.toast.error('Server Error, please do contact Admin', 'Error');
          console.log(error);
        });
    } else {
      this.toast.error('Wrong password or username', 'Error');
      return;
    }
  }
}
