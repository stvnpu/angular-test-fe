import { Component, Input } from '@angular/core';
import { FormControlOption } from '../../interfaces/form-control-option';
import { AbstractControl } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';

@Component({
  selector: 'app-error-messages',
  standalone: true,
  imports: [NgIf,NgFor],
  templateUrl: './error-messages.component.html',
  styleUrl: './error-messages.component.scss'
})
export class ErrorMessagesComponent {
  @Input() abstractControl: AbstractControl;

  @Input() formControlOption: FormControlOption;
  
}
