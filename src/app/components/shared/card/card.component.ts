import { Component, Input } from '@angular/core';
import { Employee } from '../../../interfaces/employee';
import { EmployeeService } from '../../../services/employee.service';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [],
  templateUrl: './card.component.html',
  styleUrl: './card.component.scss'
})
export class CardComponent {
@Input() data:any;
  constructor( private employeeServices : EmployeeService){}

  delete(id:string){
    console.log(id);
    this.employeeServices.deleteEmployee(id).subscribe(
      next => console.log(next),
      error => console.log(error),
    );
    console.log('delete');
  }

  update(id:string){
    console.log('update');
    
    // this.employeeServices.deleteEmployee(id);
  }

}
