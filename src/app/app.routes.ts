import { HomeComponent } from './pages/home/home.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { EditEmployeeComponent } from './pages/employee/edit/edit.component';
import { AddEmployeeComponent } from './pages/employee/add/add.component';
import { DetailEmployeeComponent } from './pages/employee/detail/detail.component';

export const routes: Routes = [
    {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'
    },
    {path:'login',component:LoginComponent},
    {path:'home',component:HomeComponent},
    {path:'employee/add',component:AddEmployeeComponent},
    {path:'employee/edit/:id',component:EditEmployeeComponent},
    {path:'employee/detail/:id',component:DetailEmployeeComponent},
    { path: '**', component: NotFoundComponent }, 
];
