import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
@Pipe({
  name: 'betterCurrency',
  standalone: true
})
export class BetterCurrencyPipe extends CurrencyPipe implements PipeTransform {
  // @ts-ignore
  transform(
    value: number | string | null | undefined,
    mode: 'positive' | 'negative' = 'positive',
    currencyCode: string = 'IDR',
    display: 'code' | 'symbol' | 'symbol-narrow' | string | boolean = 'symbol',
    digitsInfo: string = '1.0-2',
    locale: string = 'id-ID',
  ): string | null {
    const currency = super.transform(value, currencyCode, display, digitsInfo, locale);

    if (currency === null) {
      return currency;
    }

    const firstDigit = currency.search(/\d/);

    return `${currency.substring(0, firstDigit)} ${mode === 'negative' ? '-' : ''}${currency.substring(firstDigit)}`;
  }

}
