export interface FormControlOption {
  formControlName?: string; // set this value if the form control name is different from the client field
  serverField?: string; // server field used on request validation
  rules: Rule[]; // angular validation rules
}

interface Rule {
  name: string;
  message: string;
}
