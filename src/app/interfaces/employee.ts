export interface Employee {
    [key: string]: any;
    id?: string,
    username: string,
    firstName: string,
    lastName: string,
    email: string,
    birthDate: string,
    basicSalary: number,
    status: string,
    group: string,
    description: string,
}

export interface ResponseBody {
    data: Employee,
    first: number,
    items: number,
    last: number,
    next: number,
    pages: number,
    prev: number,
}