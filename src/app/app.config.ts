import { ApplicationConfig, DEFAULT_CURRENCY_CODE, LOCALE_ID } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideAnimations } from '@angular/platform-browser/animations';

import { provideToastr } from 'ngx-toastr';
import {provideNativeDateAdapter} from '@angular/material/core';
import { routes } from './app.routes';
import { provideHttpClient } from '@angular/common/http';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { environment } from '../../environment';
import { registerLocaleData } from '@angular/common';
import id from '@angular/common/locales/id';

registerLocaleData(id);
export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes), 
    provideAnimations(), 
    provideToastr({
    timeOut: 3000,
    positionClass: 'toast-bottom-right',
    preventDuplicates: true,}),
    provideHttpClient(), provideAnimationsAsync(),
    provideNativeDateAdapter(),
    {
      provide: DEFAULT_CURRENCY_CODE,
      useValue: environment.currency,
    },
    {
      provide: LOCALE_ID,
      useValue: environment.locale,
    },
]
};
